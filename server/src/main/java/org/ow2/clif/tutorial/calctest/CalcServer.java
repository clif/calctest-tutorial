package org.ow2.clif.tutorial.calctest;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Random;

/**
 * Calculator server over UDP
 * @author Bruno Dillenseger
 */
public class CalcServer implements Runnable
{
	static final String CHARSET = "US-ASCII";
	static final int PACKET_SIZE = 1024;
	static final int ERROR_CYCLE = 10;
	DatagramSocket sock;
	byte[] buffer = new byte[PACKET_SIZE];

	static public void main(String args[])
	{
		CalcServer server = null;
		try
		{
			if (args.length == 0)
			{
				server = new CalcServer();
			}
			else if (args.length == 1)
			{
				server = new CalcServer(Integer.parseInt(args[0]));
			}
		}
		catch (SocketException ex)
		{
			ex.printStackTrace(System.err);
			System.exit(-1);
		}
		System.out.println("Calculator listening on UDP socket at address " + server.getAddress());
		new Thread(server).start();
	}

	CalcServer()
	throws SocketException
	{
		sock = new DatagramSocket();
	}

	CalcServer(int port)
	throws SocketException
	{
		sock = new DatagramSocket(port);
	}

	public InetSocketAddress getAddress()
	{
		return new InetSocketAddress(sock.getLocalAddress(), sock.getLocalPort());
	}

	////////////////////////
	// Runnable interface //
	////////////////////////
	@Override
	public void run()
	{
		try
		{
			sock.setReceiveBufferSize(buffer.length);
		}
		catch (SocketException ex)
		{
			ex.printStackTrace(System.err);
			System.exit(-2);
		}
		int calls = 0;
		while (true)
		{
			try
			{
				DatagramPacket request = new DatagramPacket(buffer, buffer.length);
				sock.receive(request);
				String operation = new String(request.getData(), request.getOffset(), request.getLength(), CHARSET);
				System.out.println("request from " + request.getSocketAddress() + ": " + operation);
				int plus = operation.lastIndexOf('+');
				int minus = operation.lastIndexOf('-');
				int multiply = operation.lastIndexOf('*');
				int divide = operation.lastIndexOf('/');
				int position = (plus != -1 ? plus : (minus != -1 ? minus : (multiply != -1 ? multiply : divide)));
				String replyString;
				if (position == -1)
				{
					replyString = "invalid operation";
				}
				else
				{
					try
					{
						BigInteger result;
						BigInteger first = new BigInteger(operation.substring(0, position).trim());
						BigInteger second = new BigInteger(operation.substring(position + 1).trim());
						if (plus != -1)
						{
							result = first.add(second);
						}
						else if (minus != -1)
						{
							result = first.subtract(second);
						}
						else if (multiply != -1)
						{
							result = first.multiply(second);
						}
						else
						{
							result = first.divide(second);
						}
						if (++calls == ERROR_CYCLE)
						{
							result = result.add(BigInteger.valueOf(ERROR_CYCLE));
							calls = 0;
						}
						replyString = "=" + result;
					}
					catch (NumberFormatException ex)
					{
						replyString = "invalid number";
					}
					catch (ArithmeticException ex)
					{
						replyString = "division by zero";
					}
				}
				byte[] replyData = replyString.getBytes(CHARSET);
				BigInteger.probablePrime(64, new Random(System.currentTimeMillis()));
				DatagramPacket reply = new DatagramPacket(
					replyData,
					replyData.length,
					request.getSocketAddress());
				sock.send(reply);
				System.out.println(new String(reply.getData(), CHARSET));
			}
			catch (IOException ex)
			{
				ex.printStackTrace(System.err);
			}
		}
	}
}
