# CLIF "calculator" tutorial

## Outline

This tutorial gives you the opportunity to run a performance test with CLIF over a sample calculator service.

You will discover the main CLIF tools:
- The *CLIF server*, providing a CLIF runtime and a command-line interface for deploying, running, and controlling tests, and then collecting and analyzing measurements.
- The *CLIF web UI*, a web application providing a full-fledged web user interface over CLIF, including enhanced scenario and test plan editors.

With this tutorial, you will:
- set-up the test infrastructure, i.e. the system under test (calculator server) and the load injection system (CLIF);
- execute provided test plans;
- analyze measurements.

This tutorial provides:
- a calculator server reachable over UDP;
- a calculator client, just for manually validating connectivity with the calculator server;
- a bunch of test plans and scenarios ready-to-use for running a performance test campaign;
- the CLIF runtime and the CLIF web UI.

![Test big picture](images/calctest-overview.png)

## System under test

The System Under Test consists of a calculator service running on any machine with Java 8+ runtime. It receives simple operations on integers over UDP. Supported operations are:
- addition `+`
- subtraction `-`
- multiplication `*`
- division `/`

Requests are UDP packets whose content is a plain string of ASCII characters representing an infix-notation binary operation, such as `3+14`. The response is returned as a plain string of ASCII characters, prefixed by the `=` character. Any response not prefixed by `=` is an error message (`invalid number, invalid operation`).

Beware that some possible erroneous results would not be that surprising...

## Load injection system

The load injection system is CLIF, deployed on any machine with a Java **8** (and no other than 8) runtime. In order to get reliable results, you should use a distinct machine than the one used for the system under test. Should you use the same machine, which is no problem for running just this tutorial, remember that the load injection system may use significant computing resources (CPU, memory, disks I/O) and resulting measurements are likely to be biased - this is definitely not a recommended way for real performance tests. 

![the CLIF Load Injection Framework](images/CLIFframework.gif)

CLIF allows for deploying and controlling:
- *load injectors*, for generating requests according to a given scenario, and measuring their response times (i.e. elapsed time between the request emission and the response reception);
- *probes*, for measuring usage of some resources (CPU, memory, disks, network...)

A CLIF test is defined by the combination of:
- a *scenario* (or possibly several scenarios) specifying the flow of requests to submit to the system under test from one injection node in the network;
- a *test plan* specifying on which node(s) to deploy the scenario(s) and possibly some probes.

Scenario files take a .xis name extension, and their content follows XML format.
Test plan files take a .ctp name extension, and their content follows Java properties format.
However, we'll see that the CLIF web UI gives the opportunity to edit a YAML counterpart of these contents.

Test plans and scenarios for this tutorial are provided.

# Test set-up

## download necessary resources

[Downloading and unzipping this file](https://gitlab.ow2.org/clif/calctest-tutorial/-/jobs/artifacts/main/download?job=build_jars_and_test) will provide you with the following files:
- `calctest.zip` is a zip archive containing CLIF test plans and scenarios (keep it as a zip file if you want to import the test into the CLIF web UI);
- `calcserver.jar` is an executable Java archive for running the calculator server;
- `calcclient.jar` is an executable Java archive for running a simple calculator client, aiming at checking your calculator server set-up.

***Note.*** *All these files are automatically generated from the source files available in this repository.*

## start the system under test
- download file calcserver.jar
- get a Java 8+ runtime ready
- run this command:
  ```shell
  $ java -jar calcserver.jar 5432
  Calculator listening on UDP socket at address 0.0.0.0/0.0.0.0:5432
  ```
  ***Note.*** *Parameter 5432 sets the UDP port number for the server to listen to. This number may be changed to any free UDP port, provided the scenario parameters are updated accordingly.*

## check the system under test
- download file calcclient.jar
- run this command:
  - when on the same host as the calculator server:
    ```shell
    $ java -jar calcclient.jar localhost 5432 12-7
    =5
    ```
  - when on a remote host:
    ```shell
    $ java -jar calcclient.jar calcserver_ip_address 5432 12-7
    =5
    ```

## install CLIF runtime

CLIF requires a Java **8** (and exactly 8) runtime.

In case you have several Java versions installed on the load injection machine, arrange for Java 8 runtime to become the default one. The easiest way is to set the `PATH` environment variable with the bin subfolder of the Java 8 runtime folder at the beginning.

For example:
- Linux, MacOS
  ```shell
  $ PATH="/path/to/java8/bin:$PATH"
  ```
- Windows
  ```bat
  $ set "path=C:\path\to\java8\bin;%path%"
  ```
[Download](https://clif.ow2.io/clif-proactive/download/) the ProActive CLIF distribution from OW2.org, and then:
1. unzip the downloaded zip file,
2. unzip the extracted file `distributions/clif-packages/clif-server/target/server-3.0.4-SNAPSHOT.zip`
3. add the absolute path to the `bin` folder of the Proactive CLIF distribution to the `PATH` environment variable.

Then, check your set-up by invoking command `clifcmd`:
- Linux, MacOS
  ```shell
  $ PATH="/path/to/clif-server-3.0.4-SNAPSHOT/bin:$PATH"
  $ clifcmd version
  Linux 5.4.0-131-generic #147-Ubuntu SMP Fri Oct 14 17:07:22 UTC 2022 x86_64
  openjdk version "1.8.0_352"
  OpenJDK Runtime Environment (build 1.8.0_352-8u352-ga-1~20.04-b08)
  OpenJDK 64-Bit Server VM (build 25.352-b08, mixed mode)
  CLIF 3.0.3, built on 2020-10-19 at 16:08 by ibdi6431
  ```
- Windows
  ```
  C:\WINDOWS\system32>set "path=C:\Projets\CLIF\artifacts\clif-server-3.0.4-SNAPSHOT\bin;%path%"
  C:\WINDOWS\system32>clifcmd version
  Caption=Microsoft Windows 10 Entreprise
  OSArchitecture=64-bit
  Name=Intel(R) Core(TM) i5-10310U CPU @ 1.70GHz
  openjdk version "18-ea" 2022-03-22
  OpenJDK Runtime Environment (build 18-ea+34-2083)
  OpenJDK 64-Bit Server VM (build 18-ea+34-2083, mixed mode, sharing)
  CLIF 3.0.3, built on 2020-10-19 at 16:08 by ibdi6431
  ```
***Note.*** *Command `clifcmd` provides all features required to deploy, run and analyze tests. Try command `clifcmd help` or refer to the [command-line interface documentation](https://clif.ow2.io/doc/user_manual/manual/UserManual.pdf).*

## optional: install CLIF web UI

With the optional CLIF web UI, you will get:
- a graphical user interface instead of a command line for all operations;
- enhanced editors to read the and (possibly edit) test plan and scenario files contents.

CLIF web UI requires a Proactive CLIF runtime, as well as a Java **8** (and exactly 8) runtime (see previous section).

[Download](https://clif.ow2.io/clif-webui/download/) CLIF web UI distribution from OW2.org.  
Then, unzip the downloaded file.  
Make sure that the `PATH` environment variable contains paths to Java 8 and ProActive CLIF runtime as explained in the previous section.

Now, you are ready to launch the CLIF web UI:
```
$ bin/clifwebui
ERROR StatusLogger Log4j2 could not find a logging implementation. Please add log4j-core to the classpath. Using SimpleLogger to log to the console...

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.3.1.RELEASE)
[...]
INFOS: Starting ProtocolHandler ["http-nio-8090"]
```
The web UI is available from local host: http://localhost:8090/clifwebui

## install CLIF test plans and scenarios

Remember test plans and scenarios are available from the archive file calctest.zip downloaded together with file calcserver.jar and calcclient.jar

| interface    | instructions |
|--------------|--------------|
| command&#8209;line | Unzip calctest.zip and change your current working directory to the root folder of the unzipped contents. |
| web UI       | Import calctest.zip via the *import* feature available by right-clicking in the file browser part on the left. |

***Note.*** *The import operation unzips the calctest.zip archive into a `CLIFspace` folder created in your home folder. Then, from the `CLIFspace/calctest` folder, you may also use the CLIF command line interface.*

***Note.*** *In order to synchronize the file explorer panel of the web UI with the actual content of the `calctest` project folder, consider clicking on the CLIF icon in the upper left corner.*

Once imported or unzipped, the calctest folder contains:
- 3 test plans (*.ctp files)
- 3 scenarios (*.xis files) - 1 for each test plan
- 1 test data set file containing some operations to submit to the calculator server (`calcudp.csv`)
- 1 scenario parameters file `calcudp.props`, setting the UDP port number and the IP address of the calculator server
- some other CLIF parameters files (only file `paclif.opts` is used with ProActive CLIF, but we won't deal with this file in this tutorial)

# Running tests

## understanding scenario `calcudp1.xis`

Open scenario file `calcudp1.xis`, either with your favorite text editor, preferably with an XML syntax highlighting feature, or with CLIF web UI. In the latter case, you may benefit from both the native XML format and the YAML representation.

### plug-in import section

The first main section of a CLIF scenario is dedicated to plugin import statements.

| imported plug-ins | features |
|-------------------|----------|
| UdpInjector       | support for UDP network communication - the import parameter sets that response times are to be measured in µs |
| ConstantTimer     | timer for requests pacing |
| StringHandler     | string handling for checking the calculator results |
| Context           | read parameters defined in file `calcudp.props` |
| Common            | miscellaneous utilities (`true` constant, `log` feature for recording errors) |
| CsvProvider       | get operations from file `calcudp.csv`, to submit to the calculator server |

Plugins may provide 5 kinds of features:
- *conditions* used for conditional statements (while, if-then-else);
- *samples* used for sending a request according to some specific protocol, and measuring a response time;
- *timers* used to include arbitrary pauses (aka "think times");
- *controls* used to change some plugin settings;
- *data provisioning*, for exchanging data between plug-ins through `${pluginId:variable_name}` expressions.

You will find a complete plugins reference guide in [the CLIF documentation](https://clif.ow2.io/clif-legacy/idoc/).

### behavior(s) definition section(s)

The next main section is dedicated to defining the flow of requests that will be actually issued to the system under test by a load injection node running this scenario. CLIF uses the common concept of *virtual user*. What a virtual user will do is defined by its *behavior*.

This scenario defines a single behavior named `calc client`. After some initial configuration steps to set the UDP connection with the calculator server, the behavior loops on:
1. reading next operation from file `calcudp.csv` (`next` control)
2. submitting this operation to the calculator server (`send` sample)
3. waiting for a response (`receive` sample)
4. checking if a response has been actually received, and if it is the expected value - a record is logged in case of error
5. perform a pause whose duration is computed to meet a target pacing of one request each 50 ms (see the `period_begin - period_end` combination of timers).

### load profile section

*Load profiles* specify the number of active virtual users (or *vUsers* for short) of each defined behavior as a function of time. Here, we have a single load profile for the single behavior named `calc client`:
- it starts at time=0s with 1 vUser
- it remains running 1 vUser until time=60s
- the forceStop="true" attribute states that, when reaching the completion time of the load profile, virtual user(s) will be stopped immediately once their current instruction is complete.

***Note.*** *Should a virtual user complete its execution before the load profile ends (which is not the case here, due to the "while true" global loop), a new virtual user would be instantiated to keep the specified number of running virtual users.*
## understanding test plan calcudp1.ctp

Open test plan file `calcudp1.ctp`, either with your favorite text editor, preferably with a Java property syntax highlighting feature, or with CLIF web UI. In the latter case, you may benefit from both the native Java property format and the YAML representation.

This test plan deploys scenario `calcudp1.xis`, as well as a JVM probe that will enable for monitoring the load injector usage of Java Virtual Machine heap memory. When it comes to load testing, it is always a good idea to monitor resources usage not only on the system under test, but also on the load injection system, to make sure it is not saturating itself.

***Note.*** *Don't be misled by the trailing argument `threads=1` after the scenario file name. It does not set the number of virtual users, but it sets the size of the thread pool that will be in charge of running the scenario. It is an optimization 
parameter that will prevent the creation of 100 threads while a single one is enough.  
Refer to the [CLIF documentation](https://clif.ow2.io/doc/user_manual/manual/UserManual.pdf) for more details about tuning the scenario execution engine (so-called "ISAC execution engine").*

## executing test plan calcudp1.ctp

### using the command-line interface

1. Set your current directory to the root folder of unzipped calctest.zip
```shell
$ cd calctest
```

2. Run the test
```
$ clifcmd launch calcudp calcudp1.ctp calcudp1
Trying to connect to an existing CLIF Registry...
Connected to pnp://172.17.0.1:1234/ClifRegistry
Deploying from calcudp1.ctp test plan definition...
Socket based codeserver created on: 0.0.0.0/0.0.0.0:1357
jvm deployed on server local host
injector deployed on server local host
Test plan calcudp is deployed.
Initializing
Initialized
Starting calcudp test plan...
Started
Waiting for test termination...
Collecting data from test plan calcudp...
[Warning] Collecting results using SocketBasedFileServer implementation as property clif.fileserver.impl was not set.
Collection done
test run calcudp complete
```
The test takes about 1 minute to complete.

3. Generate the *quickstats* statistical report
```
$ clifcmd quickstats

action type	calls	min	max	mean	median	std dev	throughput	errors
LOG 	     0	   NaN	   NaN	   NaN	   NaN	   NaN	   NaN	   115
UDP CONNECT 	     1	   154	   154	154,000	   154	   NaN	   NaN	     0
UDP RECEIVE 	  1154	    13	  4477	436,841	   432	280,975	19,274	     0
UDP SEND 	  1154	    30	 10524	104,034	    95	310,938	19,271	     0
GLOBAL LOAD	  2309	    13	 10524	270,387	   125	339,757	38,555	   115
Quick statistics report done
```
Lines of interest are:
- `UDP RECEIVE`, giving statistics about response times, throughput and time-out errors on waiting for a response. Here, all requests got a response in due time, the average throughput was 19 requests per second and the average response time was 437 µs.
- `LOG`, giving the number of invalid responses (incorrect calculation results) - here, 115 requests got a bad result.

***Notes.***
- *Response times statistics, as well as the number of calls, are computed on successful requests only.*
- *`NaN` stands for "Not a Number".*
- *the columns separator is a tabulation character.*

### using the CLIF web UI

From the file explorer panel, click on file calcudp1.ctp so that it opens in the edition panel, then:
1. click on button `Deploy test` button at the upper part of the editor;
2. in the `CLIF servers` pop-up, just click on `Deploy`;
3. once the `successful deployment` pop-up appears, enter a name for this test run (or leave the default one) and initialize the test by clicking on button `Initialize`;
4. once the `successful initialization` pop-up appears, start the test by clicking on button `Start` and leave checkbox `Generate global statistical report` enabled;
5. in the monitor view, a number of statistics on metrics are displayed;
6. once the test is complete (after one minute or so), trigger measurements collection by clicking the `Collect` button;
![Quickstats screenshot from CLIF web UI](images/quickstats.png)
7. once measurements collection is complete, the quickstats report is generated and displayed. Raw measurements are stored in a dedicated subfolder of folder `report`. The quickstats report is stored in a CSV file along with this subfolder, with the same basename.
8. click on `Close` button.

## executing test plan calcudp10.ctp

This test plan deploys scenario calcudp10.xis, with a different load profile. Look at the load profile section, and go!