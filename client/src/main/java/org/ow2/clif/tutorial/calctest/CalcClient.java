package org.ow2.clif.tutorial.calctest;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

/**
 * Command-line client for sending requests to the calculator server over UDP
 * @author Bruno Dillenseger
 */
abstract public class CalcClient
{
	static final String CHARSET = "US-ASCII";
	static final int PACKET_SIZE = 1024;
	static public void main(String args[])
	{
		try
		{
			DatagramSocket socket = new DatagramSocket();
			SocketAddress serverAddr = new InetSocketAddress(
				InetAddress.getByName(args[0]),
				Integer.parseInt(args[1]));
			socket.connect(serverAddr);
			for (int i=2 ; i<args.length ; ++i)
			{
				byte[] request = args[i].getBytes(CHARSET);
				DatagramPacket packet = new DatagramPacket(request, request.length);
				socket.send(packet);
				byte[] reply = new byte[PACKET_SIZE];
				packet = new DatagramPacket(reply, reply.length);
				socket.receive(packet);
				String result = new String(
					packet.getData(),
					packet.getOffset(),
					packet.getLength(),
					CHARSET);
				System.out.println(args[i] + " " + result);
			}
			socket.close();
			System.exit(0);
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
			System.exit(-1);
		}
	}
}
